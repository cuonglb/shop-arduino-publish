﻿var map, geocoder;
var markers = [];

var adds_init = ['2 Trần Quang Diệu, Phường 1, Thành Phố Đà Lạt, Tỉnh Lâm Đồng'];
var lat_init = '10.823099';
var lng_init = '106.629664';

var isSingle = false;

var ShowMapGoogle = function (adds = null, idControlDiv = 'mapCanvas', icon_src = 'http://maps.google.com/mapfiles/ms/icons/blue.png') {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat_init, lng_init);
    var myOptions = {
        zoom: 12,
        center: latlng
    };
    map = new google.maps.Map(document.getElementById(idControlDiv), myOptions);
    var locations = adds !== null ? adds : adds_init;
    if (Array.isArray(locations)) {
        if (locations.length === 1) {
            isSingle = true;
            geocodeAddress(locations[0], icon_src);
        } else {
            for (var i = 0; i < locations.length; i++) {
                geocodeAddress(locations[i], icon_src);
            }
        }
    } else {
        isSingle = true;
        geocodeAddress(locations, icon_src);
    }
};

var geocodeAddress = function (address, icon_src) {
    //var latitude;
    //var longitude;
    geocoder.geocode({
        'address': address
    },

        function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                //latitude = results[0].geometry.location.lat();
                //longitude = results[0].geometry.location.lng();

                var mIcon = {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillOpacity: 1,
                    fillColor: 'red',
                    strokeOpacity: 1,
                    strokeWeight: 0,
                    strokeColor: '#333',
                    scale: 10
                };

                var marker = new google.maps.Marker({
                    icon: icon_src !== null ? icon_src : mIcon,
                    map: map,
                    position: results[0].geometry.location,
                    animation: google.maps.Animation.DROP
                });
                console.log(isSingle);
                //if (isSingle) {
                var infowindow = new google.maps.InfoWindow({
                    content: address, //content
                    maxWidth: 305
                });
                //infowindow.open(map, marker);
                marker.setMap(map);
                map.setCenter(marker.position);
                //} else {
                //    var bounds = map.getBounds();
                //    bounds.extend(marker.getPosition());
                //    map.fitBounds(bounds);
                //}

                google.maps.event.addListener(marker, 'click', handleMarkerClick.bind(undefined, marker, address));
            } else {
                //alert("geocode of " + address + " failed:" + status);
            }
        });
};

var handleMarkerClick = function (marker, html) {
    if (typeof iw === 'undefined') {
        iw = new google.maps.InfoWindow({
            content: html, //content
            maxWidth: 305
        });
    }
    iw.setContent(html);
    iw.open(marker.getMap(), marker);
};