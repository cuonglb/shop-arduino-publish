﻿function isNumber(evt, item) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;

    //dot
    if (iKeyCode == 46 || iKeyCode == 190) {
        return false;
    }

    if ($(item).val().length >= 2) {
        return false;
    }

    return true;
}